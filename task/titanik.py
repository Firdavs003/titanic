import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    mr_df = df["Name"].apply(lambda x: x if 'Mr.' in x else pd.NA)
    mrs_df = df["Name"].apply(lambda x: x if 'Mrs.' in x else pd.NA)
    miss_df = df["Name"].apply(lambda x: x if 'Miss.' in x else pd.NA)

    mr_NA = df[df['Name'] == mr_df]['Age'].isna().sum()
    mrs_NA = df[df['Name'] == mrs_df]['Age'].isna().sum()
    miss_NA = df[df['Name'] == miss_df]['Age'].isna().sum()

    mr_med= df[df['Name'] == mr_df]['Age'].median()
    mrs_med = df[df['Name'] == mrs_df]['Age'].median()
    miss_med = df[df['Name'] == miss_df]['Age'].median()


    return [('Mr.',mr_NA,round(mr_med)),('Mrs.',mrs_NA,round(mrs_med)),('Miss.',miss_NA,round(miss_med))]
